// ignore_for_file:  depend_on_referenced_packages

@JS()
library polkadot_lib;

import 'package:js/js.dart';

@JS()
external connectNodeJS();
external Map getBalanceJS(String address);
external List<int> getCertsJS(String address);
external List<int> getSmithCertsJS(String address);
external int getIdentityIndexJS(String address);
external Map getIdentityDataJS(int idtyIndex);
external int getCurrentUdIndexJS();
external List<List> getPastReevalsJS();
external int getIdentitiesNumberJS();
external int getMembershipNumberJS();
external List<int> getOnlineSmithsJS();
external List<int> getIncomingAuthoritiesJS();
external List<int> getOutgoingAuthoritiesJS();
external int getSmithMembershipExpirationJS(int idtyIndex);
external int getMembershipExpirationJS(int idtyIndex);
external List<int> getSessionExpirationJS(int idtyIndex);
external int newBlocsCallback();
