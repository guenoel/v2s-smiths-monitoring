// ignore_for_file: avoid_web_libraries_in_flutter
import 'dart:js_util';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:v2s_smiths_monitoring/global.dart';
import 'package:v2s_smiths_monitoring/providers/polkadot_lib.dart';
import 'dart:js' as js;

class PolkadotProvider with ChangeNotifier {
  int blocNumber = 0;
  int nbrIdentity = 0;
  int nbrMembers = 0;
  bool nodeConnected = false;
  List<String> listOnlineSmith = [];
  List<String> listIncomingAuthorities = [];
  List<String> listOutgoingAuthorities = [];
  List<String> listSmith = [];

  Future<String> asyncStartup() async {
    // Connect to duniter node using javascript interface
    final promise = connectNodeJS();
    final res = (await promiseToFuture(promise)).toString();
    if (res != 'null') {
      subscribeNewBlocs();
      nodeConnected = true;
    }

    // Init hive database
    // We're using HiveStore for persistence
    await initHiveForFlutter();

    // Get Smith pubkeys
    listOnlineSmith = await getOnlineSmithsPubkeys();
    listIncomingAuthorities = await getIncomingAuthoritiesPubkeys();
    listOutgoingAuthorities = await getOutgoingAuthoritiesPubkeys();
    //add all lists in one list
    listSmith.addAll(listIncomingAuthorities);
    listSmith.addAll(listOutgoingAuthorities);
    listSmith.addAll(listOnlineSmith);

    return res;
  }

  Future subscribeNewBlocs() async {
    var callbackJS = js.allowInterop(newBlocsCallback);
    js.context.callMethod('subscribeNewBlocsJS', [callbackJS]);
  }

  void newBlocsCallback(js.JsObject ev) {
    blocNumber = int.parse(ev['number'].toString());
    getIdentitiesNumber();
    notifyListeners();
  }

  //get single address from index
  Future<int> getIdentityIndex(String address) async {
    final idtyIndexJS = await promiseToFuture(getIdentityIndexJS(address));
    final int idtyIndex =
        int.parse(idtyIndexJS.toString() == '' ? '0' : idtyIndexJS.toString());
    return idtyIndex;
  }

  //get single index from pubkey
  Future<String> getIdentityPubkey(int idtyIndex) async {
    final Map? idtyData = idtyIndex == 0
        ? null
        : json.decode(
            (await promiseToFuture(getIdentityDataJS(idtyIndex))).toString());
    return idtyData?['ownerKey'] ?? "";
  }

  //get List of indexes from list of addresses
  Future<List<String>> listPubkeys(listIndexes) async {
    List<String> listPubkey = [];
    for (int i in listIndexes) {
      String? pubkey = await getIdentityPubkey(i);
      listPubkey.add(pubkey);
    }
    return listPubkey;
  }

  //Sum of identities
  Future<int> getIdentitiesNumber() async {
    final promise = getIdentitiesNumberJS();
    nbrIdentity = int.parse(await promiseToFuture(promise));
    return nbrIdentity;
  }

  //Sum of membership
  Future<int> getMembershipNumber() async {
    final promise = getMembershipNumberJS();
    nbrMembers = int.parse(await promiseToFuture(promise));
    return nbrMembers;
  }

  //get list of indexes from JS function
  Future<List<int>> getListIndexes(promiseListIndexesJS) async {
    final promise = promiseListIndexesJS;
    final listIndexesJS = await promiseToFuture(promise);
    final List<int> listIndexes = [];
    for (int i = 0; i < listIndexesJS.length; i++) {
      listIndexes.add(int.parse(listIndexesJS[i].toString()));
    }
    return listIndexes;
  }

  //get List of online Authorities indexes
  Future<List<int>> getOnlineSmithsIndexes() async {
    return getListIndexes(getOnlineSmithsJS());
  }

  //get List of online Authorities pubkeys
  Future<List<String>> getOnlineSmithsPubkeys() async {
    final onlineSmiths = await getOnlineSmithsIndexes();
    return listPubkeys(onlineSmiths);
  }

  Future<List<int>> getIncomingAuthoritiesIndexes() async {
    return getListIndexes(getIncomingAuthoritiesJS());
  }

  Future<List<String>> getIncomingAuthoritiesPubkeys() async {
    final incomingAuthorities = await getIncomingAuthoritiesIndexes();
    return listPubkeys(incomingAuthorities);
  }

  Future<List<int>> getOutgoingAuthoritiesIndexes() async {
    return getListIndexes(getOutgoingAuthoritiesJS());
  }

  Future<List<String>> getOutgoingAuthoritiesPubkeys() async {
    final outgoingAuthorities = await getOutgoingAuthoritiesIndexes();
    return listPubkeys(outgoingAuthorities);
  }

  Future<List<int>> getCerts(address) async {
    if (nodeConnected) {
      final promise = getCertsJS(address);
      final certsJS = (await promiseToFuture(promise));
      final certs = [
        int.parse(certsJS[0].toString()),
        int.parse(certsJS[1].toString())
      ];

      int idtyIndex = await getIdentityIndex(address);

      final promise2 = getMembershipExpirationJS(idtyIndex);
      final membershipExpirationJS = (await promiseToFuture(promise2));
      certs.add(int.parse(membershipExpirationJS.toString() == ''
          ? '0'
          : membershipExpirationJS.toString()));

      return certs;
    } else {
      return [];
    }
  }

  Future<List<int>> getSmithCerts(address) async {
    if (nodeConnected) {
      final promise = getSmithCertsJS(address);
      final smithCertsJS = (await promiseToFuture(promise));
      final smithCerts = [
        int.parse(smithCertsJS[0].toString()),
        int.parse(smithCertsJS[1].toString())
      ];

      int idtyIndex = await getIdentityIndex(address);

      final promise2 = getSmithMembershipExpirationJS(idtyIndex);
      final smithMembershipExpirationJS = (await promiseToFuture(promise2));
      smithCerts.add(int.parse(smithMembershipExpirationJS.toString() == ''
          ? '0'
          : smithMembershipExpirationJS.toString()));
      log.d(smithCerts.toString());
      return smithCerts;
    } else {
      return [];
    }
  }

  Future<List<int>> getSessionExpiration(address) async {
    if (nodeConnected) {
      int idtyIndex = await getIdentityIndex(address);

      final promise = getSessionExpirationJS(idtyIndex);
      final sessionExpirationJS = (await promiseToFuture(promise));
      final sessionExpiration = [
        int.parse(sessionExpirationJS[0].toString()),
        int.parse(sessionExpirationJS[1].toString())
      ];
      return sessionExpiration;
    } else {
      return [];
    }
  }

  Future<String> getStatus(address) async {
    String status = '';

    if (listOnlineSmith.contains(address)) {
      status = 'Online';
    }
    if (listIncomingAuthorities.contains(address)) {
      status = 'Incoming';
    }
    if (listOutgoingAuthorities.contains(address)) {
      status = 'Outgoing';
    }

    return status;
  }

  // //cette fonction est integrée dans getSmithCerts
  // Future<int> getSmithMembershipExpiration(int idtyIndex) async {
  //   final promise = getSmithMembershipExpirationJS(idtyIndex);
  //   final smithMembershipExpirationJS = (await promiseToFuture(promise));
  //   final int membershipExpiration = int.parse(
  //       smithMembershipExpirationJS.toString() == '' ? '0'
  //           : smithMembershipExpirationJS.toString());
  //   return membershipExpiration;
  // }

  // Future<int> getSmithMembershipExpirationFromPubkey(String pubkey) async {
  //   int idtyIndex = await getIdentityIndex(pubkey);
  //   return await getSmithMembershipExpiration(idtyIndex);
  // }

  // //cette fonction est integrée dans getSmithCerts
  // Future<int> getMembershipExpiration(int idtyIndex) async {
  //   final promise = getMembershipExpirationJS(idtyIndex);
  //   final membershipExpirationJS = (await promiseToFuture(promise));
  //   final int membershipExpiration = int.parse(
  //       membershipExpirationJS.toString() == '' ? '0'
  //           : membershipExpirationJS.toString());
  //   return membershipExpiration;
  // }

  // Future<int> getMembershipExpirationFromPubkey(String pubkey) async {
  //   int idtyIndex = await getIdentityIndex(pubkey);
  //   return await getMembershipExpiration(idtyIndex);
  // }
}
