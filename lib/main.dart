import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:v2s_smiths_monitoring/providers/polkadot.dart';
import 'package:v2s_smiths_monitoring/screens/smiths.dart';

// keep main as pure as possible, cause we want the app to start fast,
// and then show loading information to user
void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (_) => PolkadotProvider()),
  ], child: const SmithScreen()));
}
