import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:provider/provider.dart';
import 'package:v2s_smiths_monitoring/providers/polkadot.dart';
import 'package:v2s_smiths_monitoring/providers/polkadot_lib.dart';
import 'package:v2s_smiths_monitoring/queries.dart';
import 'package:v2s_smiths_monitoring/utils.dart';
import 'package:v2s_smiths_monitoring/global.dart';
import 'package:v2s_smiths_monitoring/widgets/builder_cert.dart';
import 'package:flutter/material.dart';
import 'package:v2s_smiths_monitoring/widgets/loading_widget.dart';
import 'package:flutter/services.dart';
import 'package:v2s_smiths_monitoring/widgets/session_expiration.dart';

class SmithScreen extends StatelessWidget {
  const SmithScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    // Use the PolkadotRiverpod class: have to change this to rivepod style

    final polka = Provider.of<PolkadotProvider>(context, listen: false);

    // Configure the GraphQL request
    final httpLink = HttpLink(
      'https://hasura.gdev.coinduf.eu/v1/graphql',
    );

    final client = ValueNotifier(GraphQLClient(
      cache: GraphQLCache(),
      link: httpLink,
    ));

    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Consumer<PolkadotProvider>(
                builder: (context, polka, _) {
                  return Text(
                    "Actual block: ${polka.blocNumber}",
                    style: const TextStyle(
                      fontSize: 30,
                      decoration: TextDecoration.none,
                      color: Colors.black,
                    ),
                  );
                },
              ),
              backgroundColor: yellowC,
            ),
            body: Container(
              //TODO getSmiths a transformer de futurebuilder en widget ?
              child: getSmiths(polka, client),
            )));
  }
}

snackCopyKey(BuildContext context, String address) {
  Clipboard.setData(ClipboardData(text: address));

  const snackBar = SnackBar(
      padding: EdgeInsets.all(20),
      content: Text("Cette adresse a été copié dans votre presse-papier.",
          style: TextStyle(fontSize: 16)),
      duration: Duration(seconds: 2));
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

FutureBuilder<String> getSmiths(
    PolkadotProvider polka, ValueNotifier<GraphQLClient> client) {
  return FutureBuilder(
      // Here we initialize every async function we need before start the app
      future: polka.asyncStartup(),
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const LoadingWidget(loadingText: 'Connection en cours ...');
        }

        return GraphQLProvider(
          client: client,
          child: Query(
            options: QueryOptions(
              document: gql(getListNameByAddressQ),
              variables: {
                'listPubKey': polka.listSmith,
              },
            ),
            builder: (QueryResult result,
                {VoidCallback? refetch, FetchMore? fetchMore}) {
              if (result.hasException) {
                return Text(result.exception.toString());
              }
              if (result.isLoading) {
                return const LoadingWidget(
                    loadingText: 'Chargement des données ...');
              }
              //debugPrint(result.data?['account'].toString());
              var res = result.data?['account'];

              if (res == '') {
                return const Text('No resultat');
              }

              List<Widget> pseudos = [];
              List<Widget> shortpubkeys = [];
              List<Widget> qrImages = [];
              List<Widget> identityIndexes = [];
              List<Widget> certs = [];
              List<Widget> smithCerts = [];
              List<Widget> sessionExpiration = [];
              List<Widget> status = [];

              for (int i = 0; i < res.length; i++) {
                String address = res[i]['pubkey']!;

                pseudos.add(
                  Text(
                    res[i]['identity']['name']!,
                    style: const TextStyle(
                        fontSize: 20,
                        decoration: TextDecoration.none,
                        color: Colors.black),
                  ),
                );
                shortpubkeys.add(
                  GestureDetector(
                    key: const Key('copyPubkey'),
                    onTap: () {
                      Clipboard.setData(ClipboardData(text: address));
                      snackCopyKey(context, address);
                    },
                    child: Text(
                      getShortPubkey(address),
                      style: const TextStyle(
                          fontSize: 20,
                          decoration: TextDecoration.none,
                          color: Colors.black),
                    ),
                  ),
                );
                qrImages.add(
                  GestureDetector(
                    key: const Key('copyPubkeyBis'),
                    onTap: () {
                      Clipboard.setData(ClipboardData(text: address));
                      snackCopyKey(context, address);
                    },
                    child: QrImageWidget(
                      data: address,
                      version: QrVersions.auto,
                      size: 120,
                    ),
                  ),
                );

                identityIndexes.add(FutureBuilder(
                    future: polka.getIdentityIndex(address),
                    builder: (context, AsyncSnapshot<int> snapshot) {
                      if (snapshot.hasData) {
                        return Text(
                          snapshot.data.toString(),
                          style: const TextStyle(
                              fontSize: 16,
                              decoration: TextDecoration.none,
                              color: Colors.black),
                        );
                      } else {
                        return const Text("waiting");
                      }
                    }));

                certs.add(
                  CertsWidget(
                    getCerts: polka.getCerts(address),
                    address: 'address',
                    icon: 'assets/medal.png',
                  ),
                );

                smithCerts.add(
                  CertsWidget(
                    getCerts: polka.getSmithCerts(address),
                    address: 'address',
                    icon: 'assets/server.png',
                  ),
                );

                // // Pas possible disocier les data de la List
                // sessionExpiration.add(FutureBuilder(
                //     future: polka.getSessionExpiration(address),
                //     builder: (context, AsyncSnapshot<List<int>> snapshot) {
                //       if (snapshot.hasData) {
                //         return Text(
                //           "Status: ${snapshot.data.toString()}",
                //           style: const TextStyle(
                //               fontSize: 20,
                //               decoration: TextDecoration.none,
                //               color: Colors.black),
                //         );
                //       } else {
                //         return const Text("waiting");
                //       }
                //     }));

                sessionExpiration.add(SessionExpirationWidget(
                    getSessionExpiration: polka.getSessionExpiration(address),
                    address: 'address'));

                status.add(FutureBuilder(
                    future: polka.getStatus(address),
                    builder: (context, AsyncSnapshot<String> snapshot) {
                      if (snapshot.hasData) {
                        return Text(
                          "Status: ${snapshot.data.toString()}",
                          style: const TextStyle(
                              fontSize: 20,
                              decoration: TextDecoration.none,
                              color: Colors.black),
                        );
                      } else {
                        return const Text("waiting");
                      }
                    }));
              }

              return SmithView(
                  pseudos: pseudos,
                  shortpubkeys: shortpubkeys,
                  qrImages: qrImages,
                  identityIndexes: identityIndexes,
                  certs: certs,
                  smithCerts: smithCerts,
                  sessionExpiration: sessionExpiration,
                  status: status);
            },
          ),
        );
      });
}

class SmithView extends StatelessWidget {
  const SmithView({
    super.key,
    required this.pseudos,
    required this.shortpubkeys,
    required this.qrImages,
    required this.identityIndexes,
    required this.certs,
    required this.smithCerts,
    required this.sessionExpiration,
    required this.status,
  });

  final List<Widget> pseudos;
  final List<Widget> shortpubkeys;
  final List<Widget> qrImages;
  final List<Widget> identityIndexes;
  final List<Widget> certs;
  final List<Widget> smithCerts;
  final List<Widget> sessionExpiration;
  final List<Widget> status;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 500,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20, // vertical spacing
            mainAxisSpacing: 20, // horizontal spacing
          ),
          itemCount: pseudos.length,
          itemBuilder: (BuildContext ctxt, int index) {
            return Card(
              color: orangeC,
              child: Center(
                child: Column(children: [
                  pseudos[index],
                  shortpubkeys[index],
                  qrImages[index],
                  identityIndexes[index],
                  certs[index],
                  smithCerts[index],
                  sessionExpiration[index],
                  status[index],
                ]),
              ),
            );
          }),
    );
  }
}
