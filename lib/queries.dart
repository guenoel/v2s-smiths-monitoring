const String getListNameByAddressQ = r'''
query ($listPubKey: [String!]) {
    account(where: {pubkey: {_in: 
        $listPubKey }} )
    {
      pubkey
      identity{
        name
      }
    } 
}
''';
