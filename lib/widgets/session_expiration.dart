import 'package:flutter/material.dart';
import 'package:v2s_smiths_monitoring/widgets/loading.dart';
import 'package:v2s_smiths_monitoring/global.dart';

class SessionExpirationWidget extends StatelessWidget {
  SessionExpirationWidget({
    required this.getSessionExpiration,
    required this.address,
    super.key,
  });

  final String address;
  final Future<List<int>> getSessionExpiration;
  Map<String, List<int>> sessionExpirationCache = {};

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getSessionExpiration,
        builder:
            (BuildContext context, AsyncSnapshot<List<int>> sessionExpiration) {
          if (sessionExpiration.hasError) {
            return const SizedBox();
          }
          if (sessionExpiration.connectionState != ConnectionState.done) {
            if (sessionExpirationCache[address] != null &&
                (sessionExpirationCache[address]![0] != 0 ||
                    sessionExpirationCache[address]![1] != 0)) {
              if (sessionExpiration.data == null) {
                return const SizedBox();
              }

              Row(
                children: [
                  Text(
                    sessionExpirationCache[address]?[0].toString() ?? '0',
                    style: const TextStyle(
                        fontSize: 20,
                        decoration: TextDecoration.none,
                        color: Colors.black),
                  ),
                  const SizedBox(width: 2),
                  Text(
                    sessionExpirationCache[address]?[1].toString() ?? '0',
                    style: const TextStyle(
                        fontSize: 20,
                        decoration: TextDecoration.none,
                        color: Colors.black),
                  ),
                ],
              );
            } else {
              return loading();
            }
          }

          sessionExpirationCache[address] = sessionExpiration.data ?? [];

          return sessionExpirationCache[address] != null &&
                  (sessionExpirationCache[address]![0] != 0 ||
                      sessionExpirationCache[address]![1] != 0)
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "expire on session ${sessionExpirationCache[address]?[0].toString() ?? '0'}",
                      style: const TextStyle(
                          fontSize: 16,
                          decoration: TextDecoration.none,
                          color: Colors.black),
                    ),
                    const SizedBox(width: 2),
                    Text(
                      "must rotate key on session ${sessionExpirationCache[address]?[1].toString() ?? '0'}",
                      style: const TextStyle(
                          fontSize: 16,
                          decoration: TextDecoration.none,
                          color: Colors.black),
                    ),
                  ],
                )
              : const Text('');
        });
  }
}
