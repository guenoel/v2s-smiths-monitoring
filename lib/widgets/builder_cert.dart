import 'package:flutter/material.dart';
import 'package:v2s_smiths_monitoring/widgets/loading.dart';
import 'package:v2s_smiths_monitoring/global.dart';

class CertsWidget extends StatelessWidget {
  CertsWidget({
    required this.getCerts,
    required this.address,
    required this.icon,
    super.key,
  });

  final String address;
  final Future<List<int>> getCerts;
  final String icon;
  Map<String, List<int>> certsCache = {};

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getCerts,
        builder: (BuildContext context, AsyncSnapshot<List<int>> certs) {
          if (certs.hasError) {
            return const SizedBox();
          }
          if (certs.connectionState != ConnectionState.done) {
            if (certsCache[address] != null &&
                (certsCache[address]![0] != 0 ||
                    certsCache[address]![1] != 0)) {
              if (certs.data == null) {
                return const SizedBox();
              }

              Row(
                children: [
                  Row(
                    children: [
                      Image.asset('assets/medal.png', height: 17),
                      const SizedBox(width: 1),
                      Column(
                        children: [
                          Text(
                            certsCache[address]?[0].toString() ?? '0',
                            style: const TextStyle(
                                fontSize: 20,
                                decoration: TextDecoration.none,
                                color: Colors.black),
                          ),
                          const SizedBox(width: 2),
                          Text(
                            certsCache[address]?[1].toString() ?? '0',
                            style: const TextStyle(
                                fontSize: 20,
                                decoration: TextDecoration.none,
                                color: Colors.black),
                          ),
                        ],
                      ),
                      const SizedBox(width: 2),
                      Image.asset('assets/arrow.png', height: 25),
                    ],
                  ),
                  Text(
                    certsCache[address]?[2].toString() ?? '0',
                    style: const TextStyle(
                        fontSize: 20,
                        decoration: TextDecoration.none,
                        color: Colors.black),
                  ),
                ],
              );
            } else {
              return loading();
            }
          }

          certsCache[address] = certs.data ?? [];

          return certsCache[address] != null &&
                  (certsCache[address]![0] != 0 || certsCache[address]![1] != 0)
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Image.asset(icon, height: 25),
                        const SizedBox(width: 10),
                        Column(
                          children: [
                            Text(
                              certsCache[address]?[0].toString() ?? '0',
                              style: const TextStyle(
                                  fontSize: 16,
                                  decoration: TextDecoration.none,
                                  color: Colors.black),
                            ),
                            const SizedBox(width: 2),
                            //for issued certifications
                            Text(
                              certsCache[address]?[1].toString() ?? '0',
                              style: const TextStyle(
                                  fontSize: 16,
                                  decoration: TextDecoration.none,
                                  color: Colors.black),
                            ),
                          ],
                        ),
                        const SizedBox(width: 2),
                        Image.asset('assets/arrow.png', height: 25),
                      ],
                    ),
                    Text(
                      "expire at block ${certsCache[address]?[2].toString() ?? '0'}",
                      style: const TextStyle(
                          fontSize: 20,
                          decoration: TextDecoration.none,
                          color: Colors.black),
                    )
                  ],
                )
              : const Text('');
        });
  }
}
