import 'package:flutter/material.dart';
import 'package:v2s_smiths_monitoring/global.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({
    required this.loadingText,
    super.key,
  });
  final String loadingText;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 100,
          width: 100,
          child: CircularProgressIndicator(
            color: orangeC,
            strokeWidth: 8,
          ),
        ),
        const SizedBox(height: 25),
        Text(
          loadingText,
          style: const TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
        )
      ],
    ));
  }
}
