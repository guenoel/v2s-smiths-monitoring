import 'package:flutter/material.dart';
import 'package:v2s_smiths_monitoring/global.dart';

Widget loading({double size = 15, double stroke = 2}) {
  return SizedBox(
    height: size,
    width: size,
    child: CircularProgressIndicator(
      color: orangeC,
      strokeWidth: stroke,
    ),
  );
}
