import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

String currencyName = 'ĞD';
late String appVersion;

// Colors
Color orangeC = const Color(0xffd07316);
Color yellowC = const Color(0xffFFD68E);
Color floattingYellow = const Color(0xffEFEFBF);
Color backgroundColor = const Color(0xFFF5F5F5);

// Screen size
late double screenWidth;
late double screenHeight;

late BuildContext homeContext;

String indexerEndpoint = '';

// Logger
final log = Logger();
