//index from address (will be parsed int)
async function getIdentityIndexJS(address) {
    return await api.query.identity.identityIndexOf(address)
}

//address from index (will be parsed String)
async function getIdentityDataJS(idtyIdenx) {
    return await api.query.identity.identities(idtyIdenx)
}

async function getCurrentUdIndexJS() {
    return await api.query.universalDividend.currentUdIndex()
}

async function getPastReevalsJS() {
    const result = (await api.query.universalDividend.pastReevals()).toString()
    const obj = JSON.parse(result)
    return obj
}

async function connectNodeJS() {
    const endpoint = "wss://gdev.p2p.legal/ws";
    console.log("Connection to endpoint " + endpoint);
    const connected = await settings.connect([endpoint]);
    return connected
}

async function getBalanceJS(address) {
    const balanceR = await api.query.system.account(address);
    return balanceR['data'];
}

//counter of indentities (will be parsed int)
async function getIdentitiesNumberJS() {
    return (await api.query.identity.counterForIdentities()).toString();
}

//counter of membership
async function getMembershipNumberJS() {
return (await api.query.smithsMembership.counterForMembership()).toString();
}

//counter of member certs received and issued (will be parsed List int)
async function getCertsJS(address) {
    const idtyIndex = await api.query.identity.identityIndexOf(address)
    const _certsReceiver = (await api.query.cert.storageIdtyCertMeta(idtyIndex.toString())).toString()
    const obj = JSON.parse(_certsReceiver);

    return [+obj['receivedCount'], +obj['issuedCount']];
}

//counter of smith certs received and issued (will be parsed List int)
async function getSmithCertsJS(address) {
    const idtyIndex = await api.query.identity.identityIndexOf(address)
    const _smithCertsReceiver = (await api.query.smithsCert.storageIdtyCertMeta(idtyIndex.toString())).toString();
    const obj = JSON.parse(_smithCertsReceiver);

    return [+obj['receivedCount'], +obj['issuedCount']];
}

//indexes of online Authorities (will be parsed List int)
async function getOnlineSmithsJS() {
    return await api.query.authorityMembers.onlineAuthorities()
}

//indexes of incoming Authorities (will be parsed List int)
async function getIncomingAuthoritiesJS() {
    return await api.query.authorityMembers.incomingAuthorities();
}

//indexes of outgoing Authorities (will be parsed List int)
async function getOutgoingAuthoritiesJS() {
    return await api.query.authorityMembers.outgoingAuthorities();
}

//Smith Membership Expiration (will be parsed int)
async function getSmithMembershipExpirationJS(idtyIdenx) {
    const smithMembershipExpiration = (await api.query.smithsMembership.membership(idtyIdenx.toString())).toString();
    obj = JSON.parse(smithMembershipExpiration);
    return obj['expireOn']
}

//Membership Expiration (will be parsed int)
async function getMembershipExpirationJS(idtyIdenx) {
    const membershipExpiration = (await api.query.membership.membership(idtyIdenx.toString())).toString();
    obj = JSON.parse(membershipExpiration);
    return obj['expireOn']
}

async function getSessionExpirationJS(idtyIdenx) {
    const sessionExpiration = (await api.query.authorityMembers.members(idtyIdenx.toString())).toString();
    obj = JSON.parse(sessionExpiration);
    return [+obj['expireOnSession'], +obj['mustRotateKeysBefore']];
}

async function subscribeNewBlocsJS(callback) {
    await api.rpc.chain.subscribeNewHeads((header) => callback(header))
}

async function getActualBlocJS() {
    return await api.rpc.chain.getHeader();
}

